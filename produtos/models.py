from django.db import models
from django.conf import settings

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    weight = models.DecimalField(max_digits=5, decimal_places=2)
    description = models.TextField(max_length=100, null=True, blank=True)
    photo = models.ImageField(upload_to='product_photos', null=True, blank=True)

    def __str__(self):
        return self.name

# Create your models here.
