from django.urls import path
from .views import product_list
from .views import product_new
from .views import product_update
from .views import product_delete
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('list/', product_list, name='product_list'),
    path('new/', product_new, name='product_new'),
    path('update/<int:id>', product_update, name='product_update'),
    path('delete/<int:id>', product_delete, name='product_delete'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

