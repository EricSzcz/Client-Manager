from django.shortcuts import render, redirect
from django.contrib.auth import logout
from produtos.models import Product

# Create your views here.
def home(request):
    products = Product.objects.all()
    return render(request, 'home.html', {'products': products})

def my_logout(request):
    logout(request)
    return redirect('home')